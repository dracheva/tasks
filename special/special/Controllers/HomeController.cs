﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using  System.Threading;
namespace special.Controllers
{
    public class HomeController : Controller
    {
        public static string[] fileNames;
        private static int index = 0;

        static HomeController()
        {
            string[] filePaths = Directory.GetFiles(System.Web.HttpContext.Current.Server.MapPath(@"~/Content/Images/"), "*.jpg");
            fileNames = filePaths.Select(x => Path.GetFileName(x)).ToArray();
        }
        public ViewResult Index()
        {
            ViewBag.AllFiles = fileNames;
            return View();
        }
       
        public PartialViewResult ChangePicture()
        {
            Thread.Sleep(3000);
            if (index + 1 == fileNames.Length)
                index = 0;
            ViewBag.Path= fileNames[index++];
            return PartialView("_Picture");
        }

        
    }
}
